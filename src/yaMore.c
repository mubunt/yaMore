//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaMore
// Yet another more (file perusal filter for xterm-like viewing)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <termios.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <sys/stat.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "yaMore_cmdline.h"
#include "pager.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define YAMORE_PACKAGE				"yaMore"
#ifdef VERSION
#define YAMORE_VERSION(x) 			str(x)
#define str(x)						#x
#else
#define YAMORE_VERSION 				"Unknown"
#endif

#define ESC 						0x1b
#define COLOR_TRAP					31	// RED

#define CMD_WILLY					"willy"
#define MD_FILESUFFIX				".md"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static const char					*labelerror	= "YAMORE:";
static const char					*template	= "./.yamoreXXXXXX";
static const char					*fmt1 		= "<center>%s v%s - From: STDIN";
static const char					*fmt2		= "<center>%s v%s - Current file: %s";
static const char					*fmt3		= "<center>%s v%s - Current file: %s [FORMATTED]";
static const char					*fmt4		= "Next file will be ""%s""";
static const char					*nomorefile = "No more file";
static struct 						sPager control;
static char							pageheader[256];
static char							nextbanner[256];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void error( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "%c[1;%dm%s %s. Abort!%c[0m\n\n", ESC, COLOR_TRAP, labelerror, buff, ESC);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void sleep_ms( int milliseconds ) {
	struct timespec ts;
	ts.tv_sec = milliseconds / 1000;
	ts.tv_nsec = (milliseconds % 1000) * 1000000;
	nanosleep(&ts, NULL);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static off_t getsize( char *file ) {
	struct stat st;
	stat(file, &st);
	return(st.st_size);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void copy_stdin( FILE *in, FILE *out ) {
	char buf[BUFSIZ];
	size_t sz;
	while ((sz = fread(&buf, sizeof(char), sizeof(buf), in)) > 0)
		fwrite(&buf, sizeof(char), sz, out);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool can_run_command(const char *cmd) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		return access(cmd, X_OK) == 0;
	}
	const char *path = getenv("PATH");
	if (!path) return false; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = malloc(strlen(path)+strlen(cmd)+3);
	for (; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for (; *path && *path!=':'; ++path,++p) *p = *path;
		// empty path entries are treated like "."
		if (p==buf) *p++='.';
		// slash and command name
		if (p[-1]!='/') *p++='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK)==0) {
			free(buf);
			return true;
		}
		// quit at last cycle
		if (!*path) break;
	}
	// not found
	free(buf);
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool ismdfile( char *file ) {
	char *pt = file + strlen(file) - 1;
	while (pt != file && *pt != '.') --pt;
	if (pt != file && strcmp(pt, MD_FILESUFFIX) == 0)
		return true;
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void *view_thread( void *in ) {
#define THREADTEMPO		500	// milliseconds
	while (1) {
		if (0 != getsize(control.filename)) break;
		sleep_ms(THREADTEMPO);
	}
	int status = pager_view(control);
	if (status != 0) pthread_exit((void *)(intptr_t)status);
	pthread_exit((void *)(intptr_t)pager_wait());
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void interrupt_handler( int dummy __attribute__((__unused__)) ) {
	signal(SIGINT, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	pager_cleaner();
	(void)unlink(control.filename);
	cmdline_parser_yaMore_free(&args_info);
	exit(EXIT_FAILURE);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int view_stdin() {
	char tmpname[128];
	strcpy(tmpname, template);
	int fd;
	pthread_t viewthread = 0;

	FILE *initialstdin = fdopen(dup(STDIN_FILENO), "r");

	if (!freopen("/dev/tty", "r", stdin)) {
		error("%s", strerror(errno));
		return EXIT_FAILURE;
	}
	if (-1 == (fd = mkstemp(tmpname))) {
		error("%s", strerror(errno));
		return EXIT_FAILURE;
	}
	signal(SIGINT, interrupt_handler);
	signal(SIGQUIT, interrupt_handler);

	snprintf(pageheader, sizeof(pageheader), fmt1, YAMORE_PACKAGE, YAMORE_VERSION(VERSION));
	if (args_info.inputs_num == 0)
		strcpy(nextbanner, nomorefile);
	else
		sprintf(nextbanner, fmt4, args_info.inputs[0]);

	control.viewtype = ASYNCHRONOUS;
	control.filename = tmpname;

	if (pthread_create(&viewthread, NULL, view_thread, (void *)NULL) != 0) {
		error("%s", strerror(errno));
		return EXIT_FAILURE;
	}

	FILE *fdt = fdopen(fd, "w+");
	copy_stdin(initialstdin, fdt);
	fclose(fdt);

	int status;
	int k = pthread_join(viewthread, (void *) &status);
	(void)unlink(tmpname);
	fclose(initialstdin);
	if (k) {
		error("%s", strerror(errno));
		status = EXIT_FAILURE;
	}
	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	return status;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int view_file( unsigned int i ) {
	snprintf(pageheader, sizeof(pageheader), fmt2, YAMORE_PACKAGE, YAMORE_VERSION(VERSION), args_info.inputs[i]);
	control.filename = args_info.inputs[i];
	return (pager_view(control));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int view_mdfile( unsigned int i ) {
	char cmd[PATH_MAX * 2];
	char tmpname[128];
	strcpy(tmpname, template);
	int fd;

	if (-1 == (fd = mkstemp(tmpname))) {
		error("%s", strerror(errno));
		return EXIT_FAILURE;
	}
	close(fd);

	snprintf(cmd, sizeof(cmd), "%s --output=%s %s", CMD_WILLY, tmpname, args_info.inputs[i]);
	int unused = system(cmd);

	snprintf(pageheader, sizeof(pageheader), fmt3, YAMORE_PACKAGE, YAMORE_VERSION(VERSION), args_info.inputs[i]);

	signal(SIGINT, interrupt_handler);
	signal(SIGQUIT, interrupt_handler);
	control.filename = tmpname;
	int n = pager_view(control);
	(void)unlink(tmpname);
	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	return n;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main( int argc, char **argv) {
	struct termios tty;
	int tty_in, tty_out, status = EXIT_SUCCESS;
	FILE *fd;
	bool mdtobeprocessed = false;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_yaMore(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;

	tty_in = tcgetattr(STDIN_FILENO, &tty);
	tty_out = tcgetattr(STDOUT_FILENO, &tty);
	// No input and no filename
	if (!tty_in && args_info.inputs_num == 0) {
		error("%s", "Missing filename");
		goto EXIT;
	}

	if (args_info.md_given) {
		if (can_run_command(CMD_WILLY))
			mdtobeprocessed = true;
		else
			error("%s", "Cannot execute 'willy' Markdown text-formatting program!");
	}
	//----  Go on --------------------------------------------------------------
	control.prefixerror = (char *) labelerror;
	//control.resizingtype = STOP_ON_RESIZING;
	control.resizingtype = REDRAW_ON_RESIZING;
	control.header = pageheader;
	control.endbanner = nextbanner;
	control.viewtype = SYNCHRONOUS;
	if (tty_in) {
		if (tty_out) {
			copy_stdin(stdin, stdout);
		} else {
			status = view_stdin();
			if (status == EXIT_FAILURE) goto EXIT;
		}
	}
	for (unsigned int i = 0 ; i < args_info.inputs_num ; i++) {
		if (i == args_info.inputs_num - 1)
			strcpy(nextbanner, nomorefile);
		else
			sprintf(nextbanner, fmt4, args_info.inputs[i + 1]);
		int n;
		if (mdtobeprocessed) {
			if (ismdfile(args_info.inputs[i]))
				n = view_mdfile(i);
			else
				n = view_file(i);
		} else
			n = view_file(i);
		if (status == EXIT_SUCCESS) status = n;
	}
	//---- Exit ----------------------------------------------------------------
	cmdline_parser_yaMore_free(&args_info);
	return status;
EXIT:
	cmdline_parser_yaMore_free(&args_info);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
