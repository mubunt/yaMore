 # *yaMore*, Yet another more (file perusal filter for xterm-like viewing).


*yaMore*  is a program similar to *more*.  *yaMore* does not have to read the entire input file before starting,
so with large input files it starts up faster than *more* or some text editors. *yaMore* run on xterm-like terminal
and is built over [**libPager**](https://gitlab.com/mubunt/libPager) library .

*yaMore* provides an option to view Markdown file formatted by [**willy**](https://gitlab.com/mubunt/willy) utility.

## EXAMPLES
![yaMore](README_images/example01.png  "Example 1")

![yaMore](README_images/example02.png  "Example 2 (with --md option)")

## LICENSE
**yaMore** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ yaMore -V
yaMore - Copyright (c) 2019, Michel RIZZO. All Rights Reserved.
yaMore - Version 1.1.0
$ yaMore -h
yaMore - Copyright (c) 2019, Michel RIZZO. All Rights Reserved.
yaMore - Version 1.1.0

Yet another more (file perusal filter for crt viewing)

Usage: yaMore [OPTIONS]... [FILE]...

  -h, --help     Print help and exit
  -V, --version  Print version and exit
      --md       Markdown formatting  (default=off)

Exit: returns a non-zero status if an error is detected.

$ yaMore main.c lib/view.c
...
$ yaMore --md main.c tests/file1.md
...

```
## STRUCTURE OF THE APPLICATION
This section walks you through **yaMore**'s structure. Once you understand this structure, you will easily find your way around in **yaMore**'s code base.

``` bash
$ yaTree
./                    # Application level
├── README_images/    # Images for documentation
│   ├── example01.png # Screenshot
│   └── example02.png # Screenshot
├── src/              # Source directory
│   ├── Makefile      # Makefile
│   ├── yaMore.c      # Source of 'yaMore' utility
│   └── yaMore.ggo    # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── tests/            # Test directory (markdown files)
│   ├── file1.md      # 
│   └── file2.md      # 
├── COPYING.md        # GNU General Public License markdown file
├── LICENSE.md        # License markdown file
├── Makefile          # Makefile
├── README.md         # ReadMe markdown file
├── RELEASENOTES.md   # Release Notes markdown file
└── VERSION           # Version identification text file

3 directories, 13 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd yaMore
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd yaMore
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## NOTES
- Refer to [**libPager**](https://gitlab.com/mubunt/libPager) library.
- Refer to [**willy**](https://gitlab.com/mubunt/willy) utility.

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
- Developped and tested on XUBUNTU 19.10, GCC v9.2.1

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***