# Header1
# Header1 with additional information
# Header1 *with additional information*
# Header1 *with additional         information   and    extra spaces*
# Lorem * ipsum * dolor sit amet,        consetetur sadipscing       elitr, sed diam nonumy eirmod-tempor-invidunt-ut-labore-et-dolor-magna-aliquyam-erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.

## Header2
### Header3
#### Header4
##### Header5
###### Header6 Lorem *ipsum* dolor sit amet,        **consetetur** sadipscing       ~~elitr, sed~~ diam nonumy eirmod tempor
Lorem * ipsum * dolor sit amet,        consetetur sadipscing       elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.

   Lorem * ipsum * dolor sit amet,        consetetur sadipscing       elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.
# Emphasis
Lorem **ipsum** dolor sit amet, *consetetur* sadipscing ~~elitr~~, **~~sed diam nonumy~~** eirmod *~~tempor invidunt ut labore~~* et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.
# Lists
* **Unordered List / First bullet:** Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.
* **Unordered List / Second bullet:**
  * **Unordered sub-list / First bullet:** Lorem **ipsum** dolor sit amet
  * **Unordered sub-list / Second bullet:** Lorem ipsum     dolor sit amet
   * **Unordered sub-sub-list:** Lorem ipsum dolor sit amet
* **Unordered List / Third bullet:**
* **Unordered List / Fourth bullet:**

**Not indented** text. Lorem * ipsum * dolor sit amet,        consetetur sadipscing       elitr, sed diam nonumy eirmod tempor
invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed
diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam
voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
no sea takimata sanctus est             Lorem ipsum dolor sit amet.
* **Unordered List / First bullet:** Lorem * ipsum * dolor sit amet...

 **Indented** text. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.
   * **Unordered sub-list / First bullet:** Lorem * ipsum * dolor sit amet,
       * **Unordered sub-sub-list / First bullet:** Lorem * ipsum * dolor sit amet.

             This is a code.

     * **Unordered sub-list / Second bullet, even if the source is not aligned with the first bullet; indentation is between the previous 2 bullets.** Lorem * ipsum * dolor sit amet,
 * **Unordered List / Second bullet, even if the source is not aligned with the first bullet; indentation is between that of the first bullet and that of the sublist** Lorem * ipsum * dolor sit amet,    
* **Unordered List / Third bullet:** Lorem * ipsum * dolor sit amet,
   * **Unordered sub-list / First bullet:**Lorem * ipsum * dolor sit amet

         First line of a code.
         Second line of a code.

* **Unordered List / Fourth bullet:** Lorem * ipsum * dolor sit amet,

      First line of a code.
      Second line of a code.
      Third line of a code.

* **Unordered List / Fifth bullet:** Lorem * ipsum * dolor sit amet,
   * **Unordered sub-list / First bullet:**Lorem * ipsum * dolor sit amet,
```
First line of a code.
Second line of a code.
```
* **Unordered List / Sixth bullet:** Lorem * ipsum * dolor sit amet,
```
First line of a code.
Second line of a code.
Third line of a code followed by an empty line.

```
 **Indented** text. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.
___
1. First ordered list item
2. Another item
  * Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.

   You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

   To have a line break without a paragraph, you will need to use two trailing spaces.
   Note that this line is separate, but within the same paragraph.
   (This is contrary to the typical GFM line break behaviour, where trailing spaces are not required

       #include "pager.h"
       int pager_view( char *filename,  char *header, enum epager_view viewtype);
       char datafile[128] = ".libpagerTest.txt"; 
       char header[128] = "<center>AUTO-TEST MODE\n<hr>\nCOLUMN 1   COLUMN 2              COLUMN 31234567890123456789012345678COLUMN 4";
       FILE *fp = NULL;      

5. Another one
* Unordered list can use asterisks
- Or minuses
+ Or pluses

5. And another one
18. Then the last.
Lorem ipsum dolor sit amet, 
19 consetetur sadipscing elitr
# Lists Again. A lot of non-ordered bullets
* Lorem ipsum dolor ...
  * Lorem ipsum dolor ...
    * Lorem ipsum dolor ...
      * Lorem ipsum dolor ...
        * Lorem ipsum dolor ...
           * Lorem ipsum dolor ...
              * Lorem ipsum dolor s...
                * Lorem ipsum dolor ...
                  * Lorem ipsum dolor ...
                    * Lorem ipsum dolor ...
                       * Lorem ipsum dolor ...
                         * Lorem ipsum dolor ...
                             * Lorem ipsum dolor ...
                               * Lorem ipsum dolor ...
                                 * Lorem ipsum dolor ...
                                   * Lorem ipsum dolor ...
                                      * Lorem ipsum dolor ...
                                         * Lorem ipsum dolor...
                                            * Lorem ipsum dolor...
                                               * Lorem ipsum dolor ...
# Lists Again. A lot of ordered bullets
1. Lorem ipsum dolor sit amet, 
2. Lorem ipsum dolor sit amet, 

0. Lorem ipsum dolor sit amet, 
1. Lorem ipsum dolor sit amet, 
  + Lorem ipsum dolor sit amet, 
2. Lorem ipsum dolor sit amet, 
   1. Lorem ipsum dolor sit amet, 
   2. Lorem ipsum dolor sit amet, 
   3. Lorem ipsum dolor sit amet, 
   4. Lorem ipsum dolor sit amet, 
      1. Lorem ipsum dolor sit amet, 
         1. Lorem ipsum dolor sit amet, 
         2. Lorem ipsum dolor sit amet, 
            1. Lorem ipsum dolor sit amet, 
              + Lorem ipsum dolor sit amet, 
              + Lorem ipsum dolor sit amet, 
            2. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
            3. Lorem ipsum dolor sit amet, 
```C
#include "pager.h"
int pager_view( char *filename,  char *header, enum epager_view viewtype);
char datafile[128] = ".libpagerTest.txt"; 
char header[128] = "<center>AUTO-TEST MODE\n<hr>\nCOLUMN 1   COLUMN 2              COLUMN 31234567890123456789012345678COLUMN 4";
FILE *fp = NULL;      
```
            3. Lorem ipsum dolor sit amet, 
         4. Lorem ipsum dolor sit amet, 
      2. Lorem ipsum dolor sit amet, 
   2. Lorem ipsum dolor sit amet, 
   4. Lorem ipsum dolor sit amet, 
1. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.

   Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam erat", sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.
 ___
   * Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,   sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam    erat",      sed diam   voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata    sanctus est.      Lorem ipsum   dolor sit amet.

     Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam         erat,   sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam    erat",      sed diam   voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata    sanctus est.      Lorem ipsum   dolor sit amet.
    ___  
    * Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,   sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam     erat",      sed diam   voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata    sanctus est.      Lorem ipsum   dolor sit amet.

      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam          erat,   sed diam voluptua. "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore        magna aliquyam    erat",      sed diam   voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata    sanctus est.      Lorem ipsum   dolor sit amet.
      ___
# Blockquotes
> ## This is a header.
> This is the first level of quoting.
> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
>
> > This is nested blockquote.
>>
>> With a second line.
> ___
> Back to the first level.
> 
> 1.   This is the first list item.
> 2.   This is the second list item.
> 
> Here's some example code:
> 
>     return shell_exec("echo $input | $markdown_script");
>
> And another code:
> ```
> return shell_exec("echo $input | $markdown_script");
> ```
> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can put Markdown into a blockquote.

# Codes
``` bash
$ yaTree
./                      # Application level
├── README_images/      # Images for documentation
│   ├── 1.png           # 
│   ├── 2.png           # 
│   ├── 3.png           #
│   ├── 4.png           #
│   └── 5.png           # 
├── src/                # Source directory
│   ├── Makefile        # Makefile
│   ├── pager.h         # 
│   ├── pager_data.h    # 
│   ├── pager_funcs.c   # 
│   ├── pager_version.c # 
│   ├── pager_view.c    # 
│   └── pager_wait.c    # 
├── COPYING             # GNU General Public License
├── LICENSE             # License file
├── Makefile            # Makefile
├── README.md           # ReadMe Mark-Down file
└── RELEASENOTES        # Release Notes text file

2 directories, 17 files
```

```C
#include "pager.h"
int pager_view( char *filename,  char *header, enum epager_view viewtype);
char datafile[128] = ".libpagerTest.txt"; 
char header[128] = "<center>AUTO-TEST MODE\n<hr>\nCOLUMN 1   COLUMN 2              COLUMN 31234567890123456789012345678COLUMN 4";
FILE *fp = NULL;      
```
# Links and Images
Refer to file [RELEASENOTES](../RELEASENOTES.md).
[Get sources](https://github.com/mubunt/libPager)
URLs and URLs in angle brackets will automatically get turned into links. 
http://www.example.com or <http://www.example.com> but not example.com.
Any URL (like http://www.github.com/) will be automatically converted into a clickable link.
This is [an example](http://example.com/ "Title") inline link.
[This link](http://example.net/) has no title attribute.
[![AUR](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/mubunt/libPager/LICENSE)
![Image 1](./1.jpeg  "Image 1")
![Image 2](./2.jpeg  "This is the second image")
## Header ![Image 1](./1.jpeg  "Image 1")
### Header [Get sources](https://github.com/mubunt/libPager)

1. [Get sources](https://github.com/mubunt/libPager)
2. [![AUR](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/mubunt/libPager/LICENSE)
3. ![Image 1](./1.jpeg  "Image 1")
4. ![Image 2](./2.jpeg  "This is the second image")

> [Get sources](https://github.com/mubunt/libPager)
>
> [![AUR](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/mubunt/libPager/LICENSE)
>
> ![Image 1](./1.jpeg  "Image 1")
>
> ![Image 2](./2.jpeg  "This is the second image")

[![AUR](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/mubunt/libPager/LICENSE)
[![AUR](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/mubunt/libPager/LICENSE "Link via image")

<https://www.google.fr>

https://www.google.fr

<https://www.google.fr>
```
https://www.google.fr
http://www.google.fr
file://www.google.fr
```
___
    https://www.google.fr
    http://www.google.fr
    file://www.google.fr
___
> ```
> https://www.google.fr
> http://www.google.fr
> file://www.google.fr
> ```
___
>> ```
>> https://www.google.fr
>> http://www.google.fr
>> file://www.google.fr
>> ```
___
>>> ```
>>> https://www.google.fr
>>> http://www.google.fr
>>> file://www.google.fr
>>> ```
___
>     https://www.google.fr
>     http://www.google.fr
>     file://www.google.fr
___
>>     https://www.google.fr
>>     http://www.google.fr
>>     file://www.google.fr
___
>>>     https://www.google.fr
>>>     http://www.google.fr
>>>     file://www.google.fr
___
1. Lorem ipsum dolor sit amet, consetetur sadipscing elitr....
```
https://www.google.fr
http://www.google.fr
file://www.google.fr
```
2. Lorem ipsum dolor sit amet, consetetur sadipscing elitr....
> ```
> https://www.google.fr
> http://www.google.fr
> file://www.google.fr
> ```
3. Lorem ipsum dolor sit amet, consetetur sadipscing elitr....
   - Lorem ipsum dolor sit amet, consetetur sadipscing elitr....
>> ```
>> https://www.google.fr
>> http://www.google.fr
>> file://www.google.fr
>> ```
   - Lorem ipsum dolor sit amet, consetetur sadipscing elitr....
>>> ```
>>> https://www.google.fr
>>> http://www.google.fr
>>> file://www.google.fr
>>> ```
___
1. Lorem ipsum dolor sit amet, consetetur sadipscing elitr....

    https://www.google.fr
    http://www.google.fr
    file://www.google.fr

2. Lorem ipsum dolor sit amet, consetetur sadipscing elitr....

>     https://www.google.fr
>     http://www.google.fr
>     file://www.google.fr

3. Lorem ipsum dolor sit amet, consetetur sadipscing elitr....
   - Lorem ipsum dolor sit amet, consetetur sadipscing elitr....

>>     https://www.google.fr
>>     http://www.google.fr
>>     file://www.google.fr

   - Lorem ipsum dolor sit amet, consetetur sadipscing elitr....

>>>     https://www.google.fr
>>>     http://www.google.fr
>>>     file://www.google.fr
___
___
https://www.google.fr
<https://www.google.fr>

![Image 1](./1.jpeg)
![Image 2](./2.jpeg  "This is the second image")
![Image 1](./1.jpeg 
"This is the second image")
![Image
2](./2.jpeg  "This is the second image")

# Tables
Markdown | Less | Pretty
--- | --- | ---
*Still* | ~~renders~~ | **nicely**
1 | 2 | 3

|Markdown
|---
|Markdown

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

Markdown | Less | Pretty
--- | --- | ---
*Still* | ~~renders~~ | **nicely**
1 | 2 | 3

|Markdown
|     ---
|*Still*
|1

Markdown|
---      |
*Still*|
1|

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | [Link](../RELEASENOTES.md) |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      | ![Image](1.jpeg  "Image") |

|:------------|--------|
|**Name**    |John Doe [Link](../RELEASENOTES.md) |
|**Position**|CEO     |


| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 | aaaaaa |
| zebra stripes | are neat      |    $1 | bbbbbb | cccccccc |

| Col1         | Col2         | Col3 |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 | Lorem ipsum dolor sit amet |
| zebra stripes | are neat      |    $1 | sed diam  | voluptua |

| Col 1         | Col 2         | Col 3  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 | Lorem ipsum dolor sit amet |
| zebra stripes | are neat|
| zebra stripes | are neat      |    $1 | voluptua | sed diam |

- Non-ordered list

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

> | Tables        | Are           | Cool  |
> | ------------- |:-------------:| -----:|
> | col 3 is      | right-aligned | $1600 |
> | col 2 is      | centered      |   $12 |
> | zebra stripes | are neat      |    $1 |

| Options | Value and meaning |
|-----------|---------------------------|
|--help, -h | Print this message and exit |
|--version, -V | Print the version information and exit |
|--xml *file*, -x *file* | Register  XML description to be used |
|--debug, -d | Debug mode |

| Legend |||
|:----------:|:------------------------------------|------------------------------------:|
| 1.          | One tab per register ![Image](1.jpeg  "Image") node defined in the XML configuration file. |  One tab per ![Image](1.jpeg  "Image") register node defined in the XML configuration file.  |
| 2.         | Register area: Display the current value of the register. The value field can be updated by user and after clicking on the "DECODE" button  or by generating a "return", the bit-field display is refreshed with this new value. | Register area: Display the current value of the register. The value field can be updated by user and after clicking on the "DECODE" button  or by generating a "return", the bit-field display is refreshed with this new value. |
| 3.        | Fields area: buttons to expand or collapse to all  levels of data detail. |Fields area: buttons to expand or collapse to all  levels of data detail. |
| 4.        | Bit-field display: display first the register and its hexadecimal value. Once expanded, bit-fields and their binary value are displayed. When a bit-field is expanded, all potential values and their description are displayed;  the current value is displayed in bold. By double-clicking on a value, the user changes the current bit-field value and then the register value. | Bit-field display: display first the register and its hexadecimal value. Once expanded, bit-fields and their binary value are displayed. When a bit-field is expanded, all potential values and their description are displayed;  the current value is displayed in bold. By double-clicking on a value, the user changes the current bit-field value and then the register value. |

___