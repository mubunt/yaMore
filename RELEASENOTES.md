# RELEASE NOTES: *yaMore*, Yet another more (file perusal filter for xterm-like viewing).

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.1.8**:
  - Updated build system components.

- **Version 1.1.7**:
  - Updated build system.

- **Version 1.1.6**:
  - Removed unused files.

- **Version 1.1.5**:
  - Updated build system component(s)

- **Version 1.1.4**:
  - Reworked build system to ease global and inter-project updated.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.1.3**:
  - Some minor changes in .comment file(s).

- **Version 1.1.2**:
	- Added option "--async | -a", but just as an experiment. Do not use it.

- **Version 1.1.1**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.1.0**:
  - Added "--md" option.

- **Version 1.0.0**:
  - First version.
